﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace WPFlashcard.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ShapeQuiz : Page
    {
        private int collection;


        private string[] urlImageCollection1 = { "ms-appx:///Assets/img/coll1_circle_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll1_cross_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll1_heart_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_hexagon_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_octagon_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_oval_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_parallelogram_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_pentagon_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_rectangle_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_square_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_star_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_triangle_quiz.jpg"
                                             };

        private string[] urlImageCollection1Ans = { "ms-appx:///Assets/img/coll1_circle.jpg", 
                                                   "ms-appx:///Assets/img/coll1_cross.jpg", 
                                                   "ms-appx:///Assets/img/coll1_heart.jpg",
                                               "ms-appx:///Assets/img/coll1_hexagon.jpg",
                                               "ms-appx:///Assets/img/coll1_octagon.jpg",
                                               "ms-appx:///Assets/img/coll1_oval.jpg",
                                               "ms-appx:///Assets/img/coll1_parallelogram.jpg",
                                               "ms-appx:///Assets/img/coll1_pentagon.jpg",
                                               "ms-appx:///Assets/img/coll1_rectangle.jpg",
                                               "ms-appx:///Assets/img/coll1_square.jpg",
                                               "ms-appx:///Assets/img/coll1_star.jpg",
                                               "ms-appx:///Assets/img/coll1_triangle.jpg"
                                             };

        private string[] urlImageCollection, urlImageCollectionAns;
        private int[] flags;

        public ShapeQuiz()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            collection = (int)e.Parameter;

            switch (collection)
            {
                case 1:
                    urlImageCollection = urlImageCollection1;
                    urlImageCollectionAns = urlImageCollection1Ans;
                    break;

            }
            pivotData.ItemsSource = urlImageCollection;
            flags = new int[urlImageCollection.Length];
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame == null)
            {
                return;
            }

            if (frame.CanGoBack)
            {
                frame.GoBack();
                e.Handled = true;
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            int index = pivotData.SelectedIndex;

            if (flags[index] != 1)
            {
                var answerImage = new BitmapImage(new Uri(urlImageCollectionAns[index]));
                Image img = sender as Image;
                img.Source = answerImage;
                flags[index] = 1;
            }
        }
    }
}
