﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace WPFlashcard.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TransportQuiz : Page
    {
        private int collection;

        private string[] urlImageCollection1 = { "ms-appx:///Assets/img/coll1_ambulance_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll1_ballon_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll1_bicycle_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_boat_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_bus_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_car_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_helicopter_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_motor_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_plane_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_spaceship_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_submarine_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_train_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_truck_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_zeppelin_quiz.jpg"
                                             };

        private string[] urlImageCollection1Ans = { "ms-appx:///Assets/img/coll1_ambulance.jpg", 
                                                   "ms-appx:///Assets/img/coll1_ballon.jpg", 
                                                   "ms-appx:///Assets/img/coll1_bicycle.jpg",
                                               "ms-appx:///Assets/img/coll1_boat.jpg",
                                               "ms-appx:///Assets/img/coll1_bus.jpg",
                                               "ms-appx:///Assets/img/coll1_car.jpg",
                                               "ms-appx:///Assets/img/coll1_helicopter.jpg",
                                               "ms-appx:///Assets/img/coll1_motor.jpg",
                                               "ms-appx:///Assets/img/coll1_plane.jpg",
                                               "ms-appx:///Assets/img/coll1_spaceship.jpg",
                                               "ms-appx:///Assets/img/coll1_submarine.jpg",
                                               "ms-appx:///Assets/img/coll1_train.jpg",
                                               "ms-appx:///Assets/img/coll1_truck.jpg",
                                               "ms-appx:///Assets/img/coll1_zeppelin.jpg"
                                             };

        private string[] urlImageCollection, urlImageCollectionAns;
        private int[] flags;

        public TransportQuiz()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            collection = (int)e.Parameter;

            switch (collection)
            {
                case 1:
                    urlImageCollection = urlImageCollection1;
                    urlImageCollectionAns = urlImageCollection1Ans;
                    break;
               
            }
            pivotData.ItemsSource = urlImageCollection;
            flags = new int[urlImageCollection.Length];
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame == null)
            {
                return;
            }

            if (frame.CanGoBack)
            {
                frame.GoBack();
                e.Handled = true;
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            int index = pivotData.SelectedIndex;

            if (flags[index] != 1)
            {
                var answerImage = new BitmapImage(new Uri(urlImageCollectionAns[index]));
                Image img = sender as Image;
                img.Source = answerImage;
                flags[index] = 1;
            }
        }
    }
}
