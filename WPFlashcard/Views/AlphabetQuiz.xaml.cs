﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace WPFlashcard.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AlphabetQuiz : Page
    {
        private int collection;
        private string[] urlImageCollection1 = { "ms-appx:///Assets/img/coll1_a_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll1_b_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll1_c_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_d_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_e_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_f_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_g_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_h_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_i_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_j_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_k_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_l_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_m_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_n_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_o_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_p_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_q_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_r_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_s_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_t_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_u_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_v_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_w_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_x_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_y_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_z_quiz.PNG"};

        private string[] urlImageCollection1Ans = { "ms-appx:///Assets/img/coll1_a.PNG", 
                                                   "ms-appx:///Assets/img/coll1_b.PNG", 
                                                   "ms-appx:///Assets/img/coll1_c.PNG",
                                               "ms-appx:///Assets/img/coll1_d.PNG",
                                               "ms-appx:///Assets/img/coll1_e.PNG",
                                               "ms-appx:///Assets/img/coll1_f.PNG",
                                               "ms-appx:///Assets/img/coll1_g.PNG",
                                               "ms-appx:///Assets/img/coll1_h.PNG",
                                               "ms-appx:///Assets/img/coll1_i.PNG",
                                               "ms-appx:///Assets/img/coll1_j.PNG",
                                               "ms-appx:///Assets/img/coll1_k.PNG",
                                               "ms-appx:///Assets/img/coll1_l.PNG",
                                               "ms-appx:///Assets/img/coll1_m.PNG",
                                               "ms-appx:///Assets/img/coll1_n.PNG",
                                               "ms-appx:///Assets/img/coll1_o.PNG",
                                               "ms-appx:///Assets/img/coll1_p.PNG",
                                               "ms-appx:///Assets/img/coll1_q.PNG",
                                               "ms-appx:///Assets/img/coll1_r.PNG",
                                               "ms-appx:///Assets/img/coll1_s.PNG",
                                               "ms-appx:///Assets/img/coll1_t.PNG",
                                               "ms-appx:///Assets/img/coll1_u.PNG",
                                               "ms-appx:///Assets/img/coll1_v.PNG",
                                               "ms-appx:///Assets/img/coll1_w.PNG",
                                               "ms-appx:///Assets/img/coll1_x.PNG",
                                               "ms-appx:///Assets/img/coll1_y.PNG",
                                               "ms-appx:///Assets/img/coll1_z.PNG"};

        private string[] urlImageCollection3 = { "ms-appx:///Assets/img/coll4_a_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll4_b_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll4_c_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_d_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_e_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_f_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_g_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_h_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_i_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_j_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_k_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_l_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_m_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_n_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_o_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_p_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_q_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_r_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_s_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_t_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_u_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_v_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_w_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_x_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_y_quiz.PNG",
                                               "ms-appx:///Assets/img/coll4_z_quiz.PNG"};

        private string[] urlImageCollection3Ans = { "ms-appx:///Assets/img/coll4_a.PNG", 
                                                   "ms-appx:///Assets/img/coll4_b.PNG", 
                                                   "ms-appx:///Assets/img/coll4_c.PNG",
                                               "ms-appx:///Assets/img/coll4_d.PNG",
                                               "ms-appx:///Assets/img/coll4_e.PNG",
                                               "ms-appx:///Assets/img/coll4_f.PNG",
                                               "ms-appx:///Assets/img/coll4_g.PNG",
                                               "ms-appx:///Assets/img/coll4_h.PNG",
                                               "ms-appx:///Assets/img/coll4_i.PNG",
                                               "ms-appx:///Assets/img/coll4_j.PNG",
                                               "ms-appx:///Assets/img/coll4_k.PNG",
                                               "ms-appx:///Assets/img/coll4_l.PNG",
                                               "ms-appx:///Assets/img/coll4_m.PNG",
                                               "ms-appx:///Assets/img/coll4_n.PNG",
                                               "ms-appx:///Assets/img/coll4_o.PNG",
                                               "ms-appx:///Assets/img/coll4_p.PNG",
                                               "ms-appx:///Assets/img/coll4_q.PNG",
                                               "ms-appx:///Assets/img/coll4_r.PNG",
                                               "ms-appx:///Assets/img/coll4_s.PNG",
                                               "ms-appx:///Assets/img/coll4_t.PNG",
                                               "ms-appx:///Assets/img/coll4_u.PNG",
                                               "ms-appx:///Assets/img/coll4_v.PNG",
                                               "ms-appx:///Assets/img/coll4_w.PNG",
                                               "ms-appx:///Assets/img/coll4_x.PNG",
                                               "ms-appx:///Assets/img/coll4_y.PNG",
                                               "ms-appx:///Assets/img/coll4_z.PNG"};

        private string[] urlImageCollection, urlImageCollectionAns;
        private int[] flags;
        public AlphabetQuiz()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            collection = (int)e.Parameter;

            switch (collection)
            {
                case 1:
                    urlImageCollection = urlImageCollection1;
                    urlImageCollectionAns = urlImageCollection1Ans;
                    break;
                case 3:
                    urlImageCollection = urlImageCollection3;
                    urlImageCollectionAns = urlImageCollection3Ans;
                    break;
            }
            pivotData.ItemsSource = urlImageCollection;
            flags = new int[urlImageCollection.Length];
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame == null)
            {
                return;
            }

            if (frame.CanGoBack)
            {
                frame.GoBack();
                e.Handled = true;
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            int index = pivotData.SelectedIndex;

            if(flags[index] != 1)
            {
                var answerImage = new BitmapImage(new Uri(urlImageCollectionAns[index]));
                Image img = sender as Image;
                img.Source = answerImage;
                flags[index] = 1;
            }
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            Image img = sender as Image;

            if (img == null)
                return;

            Storyboard s = new Storyboard();

            DoubleAnimation doubleAni = new DoubleAnimation();
            doubleAni.To = 1;
            doubleAni.Duration = new Duration(TimeSpan.FromMilliseconds(2000));

            Storyboard.SetTarget(doubleAni, img);
            Storyboard.SetTargetProperty(doubleAni, "Opacity");

            s.Children.Add(doubleAni);
            s.Begin();
        }
    }
}
