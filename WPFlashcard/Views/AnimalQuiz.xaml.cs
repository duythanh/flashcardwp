﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace WPFlashcard.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AnimalQuiz : Page
    {
        private int collection;

        private string[] urlImageCollection1 = { "ms-appx:///Assets/img/coll1_crab_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll1_dolphin_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll3_3_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_octopus_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_penguin_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_seahorse_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_shark_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_starfish_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_turtle_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_walrus_quiz.jpg",
                                               "ms-appx:///Assets/img/coll1_whale_quiz.jpg"
                                             };

        private string[] urlImageCollection1Ans = { "ms-appx:///Assets/img/coll1_crab.jpg", 
                                                   "ms-appx:///Assets/img/coll1_dolphin.jpg", 
                                                   "ms-appx:///Assets/img/coll3_3.jpg",
                                               "ms-appx:///Assets/img/coll1_octopus.jpg",
                                               "ms-appx:///Assets/img/coll1_penguin.jpg",
                                               "ms-appx:///Assets/img/coll1_seahorse.jpg",
                                               "ms-appx:///Assets/img/coll1_shark.jpg",
                                               "ms-appx:///Assets/img/coll1_starfish.jpg",
                                               "ms-appx:///Assets/img/coll1_turtle.jpg",
                                               "ms-appx:///Assets/img/coll1_walrus.jpg",
                                               "ms-appx:///Assets/img/coll1_whale.jpg"
                                             };

        private string[] urlImageCollection2 = { "ms-appx:///Assets/img/coll2_chameleon_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll2_chicken_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll2_crocodile_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_deer_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_dinosaur_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_elephant_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_frog_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_goose_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_lion_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_monkey_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_rhino_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_squirrel_quiz.jpg",
                                               "ms-appx:///Assets/img/coll2_tiger_quiz.jpg"
                                             };

        private string[] urlImageCollection2Ans = { "ms-appx:///Assets/img/coll2_chameleon.jpg", 
                                                   "ms-appx:///Assets/img/coll2_chicken.jpg", 
                                                   "ms-appx:///Assets/img/coll2_crocodile.jpg",
                                               "ms-appx:///Assets/img/coll2_deer.jpg",
                                               "ms-appx:///Assets/img/coll2_dinosaur.jpg",
                                               "ms-appx:///Assets/img/coll2_elephant.jpg",
                                               "ms-appx:///Assets/img/coll2_frog.jpg",
                                               "ms-appx:///Assets/img/coll2_goose.jpg",
                                               "ms-appx:///Assets/img/coll2_lion.jpg",
                                               "ms-appx:///Assets/img/coll2_monkey.jpg",
                                               "ms-appx:///Assets/img/coll2_rhino.jpg",
                                               "ms-appx:///Assets/img/coll2_squirrel.jpg",
                                               "ms-appx:///Assets/img/coll2_tiger.jpg"
                                             };

        private string[] urlImageCollection, urlImageCollectionAns;
        private int[] flags;

        public AnimalQuiz()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            collection = (int)e.Parameter;

            switch (collection)
            {
                case 1:
                    urlImageCollection = urlImageCollection1;
                    urlImageCollectionAns = urlImageCollection1Ans;
                    break;
                case 2:
                    urlImageCollection = urlImageCollection2;
                    urlImageCollectionAns = urlImageCollection2Ans;
                    break;
            }
            pivotData.ItemsSource = urlImageCollection;
            flags = new int[urlImageCollection.Length];
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame == null)
            {
                return;
            }

            if (frame.CanGoBack)
            {
                frame.GoBack();
                e.Handled = true;
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            int index = pivotData.SelectedIndex;

            if (flags[index] != 1)
            {
                var answerImage = new BitmapImage(new Uri(urlImageCollectionAns[index]));
                Image img = sender as Image;
                img.Source = answerImage;
                flags[index] = 1;
            }
        }
    }
}
