﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace WPFlashcard.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NumberQuiz : Page
    {
        private int collection;
        private string[] urlImageCollection1 = { "ms-appx:///Assets/img/coll1_1_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll1_2_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll1_3_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_4_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_5_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_6_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_7_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_8_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_9_quiz.PNG",
                                               "ms-appx:///Assets/img/coll1_10_quiz.PNG"
                                             };

        private string[] urlImageCollection1Ans = { "ms-appx:///Assets/img/coll1_1.PNG", 
                                                   "ms-appx:///Assets/img/coll1_2.PNG", 
                                                   "ms-appx:///Assets/img/coll1_3.PNG",
                                               "ms-appx:///Assets/img/coll1_4.PNG",
                                               "ms-appx:///Assets/img/coll1_5.PNG",
                                               "ms-appx:///Assets/img/coll1_6.PNG",
                                               "ms-appx:///Assets/img/coll1_7.PNG",
                                               "ms-appx:///Assets/img/coll1_8.PNG",
                                               "ms-appx:///Assets/img/coll1_9.PNG",
                                               "ms-appx:///Assets/img/coll1_10.PNG"
                                             };

        private string[] urlImageCollection2 = { "ms-appx:///Assets/img/coll2_1_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll2_2_quiz.PNG", 
                                                   "ms-appx:///Assets/img/coll2_3_quiz.PNG",
                                               "ms-appx:///Assets/img/coll2_4_quiz.PNG",
                                               "ms-appx:///Assets/img/coll2_5_quiz.PNG",
                                               "ms-appx:///Assets/img/coll2_6_quiz.PNG",
                                               "ms-appx:///Assets/img/coll2_7_quiz.PNG",
                                               "ms-appx:///Assets/img/coll2_8_quiz.PNG",
                                               "ms-appx:///Assets/img/coll2_9_quiz.PNG",
                                               "ms-appx:///Assets/img/coll2_10_quiz.PNG"
                                             };

        private string[] urlImageCollection2Ans = { "ms-appx:///Assets/img/coll2_1.PNG", 
                                                   "ms-appx:///Assets/img/coll2_2.PNG", 
                                                   "ms-appx:///Assets/img/coll2_3.PNG",
                                               "ms-appx:///Assets/img/coll2_4.PNG",
                                               "ms-appx:///Assets/img/coll2_5.PNG",
                                               "ms-appx:///Assets/img/coll2_6.PNG",
                                               "ms-appx:///Assets/img/coll2_7.PNG",
                                               "ms-appx:///Assets/img/coll2_8.PNG",
                                               "ms-appx:///Assets/img/coll2_9.PNG",
                                               "ms-appx:///Assets/img/coll2_10.PNG"
                                             };

        private string[] urlImageCollection3 = { "ms-appx:///Assets/img/coll3_1_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll3_2_quiz.jpg", 
                                                   "ms-appx:///Assets/img/coll3_3_quiz.jpg",
                                               "ms-appx:///Assets/img/coll3_4_quiz.jpg",
                                               "ms-appx:///Assets/img/coll3_5_quiz.jpg",
                                               "ms-appx:///Assets/img/coll3_6_quiz.jpg",
                                               "ms-appx:///Assets/img/coll3_7_quiz.jpg",
                                               "ms-appx:///Assets/img/coll3_8_quiz.jpg",
                                               "ms-appx:///Assets/img/coll3_9_quiz.jpg",
                                               "ms-appx:///Assets/img/coll3_10_quiz.jpg"
                                             };

        private string[] urlImageCollection3Ans = { "ms-appx:///Assets/img/coll3_1.jpg", 
                                                   "ms-appx:///Assets/img/coll3_2.jpg", 
                                                   "ms-appx:///Assets/img/coll3_3.jpg",
                                               "ms-appx:///Assets/img/coll3_4.jpg",
                                               "ms-appx:///Assets/img/coll3_5.jpg",
                                               "ms-appx:///Assets/img/coll3_6.jpg",
                                               "ms-appx:///Assets/img/coll3_7.jpg",
                                               "ms-appx:///Assets/img/coll3_8.jpg",
                                               "ms-appx:///Assets/img/coll3_9.jpg",
                                               "ms-appx:///Assets/img/coll3_10.jpg"
                                             };

        private string[] urlImageCollection, urlImageCollectionAns;
        private int[] flags;
        public NumberQuiz()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            collection = (int)e.Parameter;

            switch (collection)
            {
                case 1:
                    urlImageCollection = urlImageCollection1;
                    urlImageCollectionAns = urlImageCollection1Ans;
                    break;
                case 2:
                    urlImageCollection = urlImageCollection2;
                    urlImageCollectionAns = urlImageCollection2Ans;
                    break;
                case 3:
                    urlImageCollection = urlImageCollection3;
                    urlImageCollectionAns = urlImageCollection3Ans;
                    break;
            }
            pivotData.ItemsSource = urlImageCollection;
            flags = new int[urlImageCollection.Length];
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame == null)
            {
                return;
            }

            if (frame.CanGoBack)
            {
                frame.GoBack();
                e.Handled = true;
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            int index = pivotData.SelectedIndex;

            if (flags[index] != 1)
            {
                var answerImage = new BitmapImage(new Uri(urlImageCollectionAns[index]));
                Image img = sender as Image;
                img.Source = answerImage;
                flags[index] = 1;
            }
        }
    }
}
